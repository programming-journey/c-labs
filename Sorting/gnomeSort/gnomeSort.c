void gnome_sort(int A[], int n) {
	int i = 0;
	while (i < n) { 
		if (i == 0 || A[i-1] <= A[i]) {
		i++;
		}
		else {
			int tmp = A[i];
			A[i] = A[i-1];
			A[i-1] = tmp;
			i--;
		}
	}
}
