#Gnome Sort

- Time Complexity
  - O(n^{2})
  - \Omega(n)
- Space Complexity
  - O(1)

## Theory
This algorithm find first place where two adjacent elements are in th wrong order and swap them, then it will introduce new out-of-order adjacent pairs.

## Use Case
This average running time is almost O(n) if the list is __almost sorted__.
