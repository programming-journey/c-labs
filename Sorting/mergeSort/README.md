#Merge Sort

- Time Complexity
  - O(nlog(n))
- Space Complexity
  - O(n)



## Use Case
This is a __generally fast__ algorithm