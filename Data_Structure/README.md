# Data Structures in C language
This is the portal saving the data structures
## The Data Structures Presented Here
- [x] Stack
- [x] Queue
- [x] Hash table
- [x] Lists 
- [ ] Binary Search Tree
- [ ] AVL Trees
- [ ] Express Trees
- [ ] Priority Queues
- [ ] B-Tree
- [ ] Splay Tree
- [ ] Red Black Trees
- [ ] Disjoint Sets
- [ ] Graph

