//
// Created by Alan on 2019-03-16.
//
#include "BinaryTree.h"

/* High level manipulation */
BinaryTreeADT InsertNode(BinaryTreeADT t, TreeNodeADT n);
TreeNodeADT FindMinNode(BinaryTreeADT t);
BinaryTreeADT DeleteNode(BinaryTreeADT t, TreeNodeADT n);
