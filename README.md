1. Hello World
- [x] Print Hello World to the console
2. Anagrams
- [x] Take a string through the console input
- [x] Take in a file with a dictionary of useable words.
- [x] Output a list of words to the console that are an anagram of the inputted string
- [ ] Be Tested!

3. The pizza app
- [x] User can select a pizza and add to basket.
- [x] Basket displays a subtotal.
- [x] Basket displays the tax amount (% of your choice).
- [x] Prints a list of pizzas on the menu to the console.

credit: https://medium.com/@samuel.fare/want-to-learn-any-programming-language-write-these-3-simple-apps-5af8cd119921